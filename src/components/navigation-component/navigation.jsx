import React, {Component , Fragment} from "react";
import style from './navigation.scss';

export class Navigation extends Component{

    render() {
        return(
            <Fragment>
                <video autoPlay muted loop className={style.myVideo}>
                    <source src="/assets/images/HEXORIA EXPLAINER FIN_1_1.mp4" type="video/mp4"/>
                </video>

                <div className={style.content}>
                    <h1>Heading</h1>
                    <button type="button" className="btn btn-link">Why HEROXIA</button>
                    <button type="button" className="btn btn-link">Benefits</button>
                    <button type="button" className="btn btn-link">Advantages</button>

                </div>

                <div className={style.middleContent}>
                    <h1>Heroxia - We are the place <br/> for private events</h1>
                    <br/>
                    <p>
                        Discover the magical place where like-minded people celebrate   <br/>
                        life and enjoy the moment.Create and experience your moment with Heroxia

                    </p>

                </div>
            </Fragment>
        )
    }
}
export default Navigation;
