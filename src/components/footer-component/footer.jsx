import React, {Component , Fragment} from "react";
import style from './footer.scss';

export class Footer extends Component {

    getCurrentOrganizerState = () => {
        const {changeOrganizerImageState , activeOrganizer} = this.props;
        let stateOfImage;
        stateOfImage = !activeOrganizer;
        changeOrganizerImageState(stateOfImage)
    };

    getCurrentVisitorState = () => {
        const {changeVisitorImageState , activeVisitor} = this.props;
        let stateOfImage;
        stateOfImage = !activeVisitor;
        changeVisitorImageState(stateOfImage)
    };

    render() {
        const {activeOrganizer , activeVisitor} = this.props;
        return(
            <Fragment>
                <div className={style.footerContainer}>
                    <div className={style.imageContainer}>
                        <p className={style.largeParagraph}>Become out first friend now and you will <br/> one-year free premium account</p>
                        <p className={style.smallParagraph}>In what role would you like to use your application</p>
                        <div className={`${style.images} row no-gutters`}>
                            <div className={`col-6`} >
                                <div  onClick={()=>this.getCurrentOrganizerState()}>
                                    {
                                        activeOrganizer === true ?
                                            <img src="/assets/images/Inacctive_organizer_bt.svg" alt="" /> :
                                            <img src="/assets/images/Active_organizer_bt.svg" alt="" />
                                    }
                                    <p className={style.largeParagraph}>Event Organizer</p>
                                </div>

                            </div>
                            <div className={`col-6`} >
                                <div  onClick={()=>this.getCurrentVisitorState()}>
                                    {
                                        activeVisitor === true ?
                                            <img src="/assets/images/Inactive_visitor_bt.svg" alt="" /> :
                                            <img src="/assets/images/Active_visitor_bt.svg" alt="" />
                                    }
                                    <p className={style.largeParagraph}>Event Visitor</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )

    }
}

