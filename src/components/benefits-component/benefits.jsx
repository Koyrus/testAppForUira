import React, {Component , Fragment} from "react";
import style from './benefits.scss';

export class Benefits extends Component{

    render() {
        return(
            <Fragment>
                <div className={`${style.benefitsContainer} row no-gutters`}>
                    <div className={`col-4`}>
                        <p>Benefits</p>
                        <div >
                            <ul>
                                <li>
                                    <img src="/assets/images/Community_ic.svg" alt=""/>
                                    <span>Be part of extraordinary and private events like never before</span>
                                </li>
                                <li>
                                    <img src="/assets/images/Event_ic.svg" alt=""/>
                                    <span>Create simple & safe events for your occasion</span>
                                </li>
                                <li>
                                    <img src="/assets/images/Safe_ic.svg" alt=""/>
                                    <span>Express yourself & enjoy your time with likeminded people</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className={`col-8`}>
                        <img src="/assets/images/Event_creat.png" alt=""/>
                    </div>
                </div>

            </Fragment>
        )
    }
}
export default Benefits;
