import React, {Component , Fragment} from "react";
import style from './description.scss';

export class Description extends Component{

    render() {
        return(
            <Fragment>
                <div className={style.mainContainer}>
                    <div className={`row no-gutters`} >
                        <div className={`col-6`}>
                            <img src="/assets/images/Wy%20_hexoria.png" alt=""/>
                        </div>
                        <div className={`col-6`}>
                            <div className={style.descriptionContainer}>
                                <h1>Why Heroxia?</h1>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                </p>
                            </div>

                        </div>

                    </div>
                </div>
            </Fragment>
        )
    }
}
export default Description;
