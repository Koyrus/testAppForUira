import React, {Component, Fragment} from 'react';
import style from './App.scss';
import Description from "../why-us-component/description";
import Navigation from "../navigation-component/navigation";
import Second from "../second-video-component/second-video";
import Benefits from "../benefits-component/benefits";
import Advantages from "../advantages-component/advantages";
import {Footer} from "../footer-component/footer";

export class App extends Component{
  constructor(props){
    super(props);
      this.state = {
        activeOrganizer : true,
        activeVisitor : true
      }
  }

  changeVisitorImageState = () =>  this.setState({activeVisitor : !this.state.activeVisitor});

  changeOrganizerImageState = () => this.setState({activeOrganizer : !this.state.activeOrganizer});

  render() {
      let {activeOrganizer , activeVisitor} = this.state;
    return(
        <Fragment>
            <div className={style.videoFragment}>
                <Navigation/>
            </div>
            <Description/>
            <div className={style.secondVideoFragment}>
                <Second/>
            </div>
            <Benefits/>
            <Advantages/>
            <br/>
            <Footer changeVisitorImageState={this.changeVisitorImageState}
                    changeOrganizerImageState={this.changeOrganizerImageState}
                    activeVisitor={activeVisitor}
                    activeOrganizer={activeOrganizer}
            />
        </Fragment>
    )
  }

}
