import React, {Component , Fragment} from "react";
import style from './advantages.scss';

export class Advantages extends Component{
    render() {
        return(
            <Fragment>
                <div className={style.advantagesContainer}>
                    <p>Advantages</p>
                    <div className={style.iconsContainer}>
                        <div className={`row no-gutters`} style={{padding: '10px'}}>
                            <div className={`col-4`}>
                                <img src="/assets/images/Alarm Schedule.svg" alt=""/>
                                <p className={style.boldParagraph}>
                                    Save time & money
                                </p>
                                <span className={style.smallSpan}>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </span>

                            </div>
                            <div className={`col-4`}>
                                <img src="/assets/images/Finance.svg" alt=""/>
                                <p className={style.boldParagraph}>
                                    Make money
                                </p>
                                <span className={style.smallSpan}>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </span>
                            </div>
                            <div className={`col-4`}>
                                <img src="/assets/images/Security.svg" alt=""/>
                                <p className={style.boldParagraph}>
                                    Security
                                </p>
                                <span className={style.smallSpan}>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </span>

                            </div>

                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
export default Advantages;
