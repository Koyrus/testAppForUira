import React, {Component, Fragment} from 'react';
import style from './second-video.scss';

export class Second extends Component{

    render() {
        return(
            <Fragment>
                <video width={1200} autoPlay muted loop className={style.videoContainer} id={'second'} name={'video'}>
                    <source src="/assets/images/HEXORIA_TEASER_MOVIE.mp4" type="video/mp4"/>
                </video>
            </Fragment>




        )
    }

}
export default Second;
